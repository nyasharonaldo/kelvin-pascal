package com.citi.training.trader.rest;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.service.PriceService;
import com.citi.training.trader.service.SimpleStrategyService;
import com.citi.training.trader.service.StockService;

/**
 * REST Controller for {@link com.citi.training.trader.model.SimpleStrategy}
 * resource.
 *
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/strategies")
public class SimpleStrategyController {

	private static final Logger LOG = LoggerFactory.getLogger(SimpleStrategyController.class);

	@Autowired
	private SimpleStrategyService simpleStrategyService;

	@Autowired
	private StockService stockService;

	@Autowired
	private PriceService priceService;

	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<SimpleStrategy> findAll() {
		LOG.info("findAll()");
		return simpleStrategyService.findAll();
	}


	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public SimpleStrategy findById(@PathVariable int id) {
		LOG.info("findById [" + id + "]");
		return simpleStrategyService.findById(id);
	}


	@RequestMapping(value = "/findLatest", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public SimpleStrategy findLatest() {
		LOG.info("getting latest strategy");
		return simpleStrategyService.findLatest();
	}
	
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<SimpleStrategy> create(@RequestBody SimpleStrategy simpleStrategy) {
		LOG.info("HTTP POST : create[" + simpleStrategy + "]");

		SimpleStrategy createdSimpleStrategy = simpleStrategyService.save(simpleStrategy);
		LOG.info("created simpleStrategy: [" + createdSimpleStrategy + "]");

		return new ResponseEntity<SimpleStrategy>(createdSimpleStrategy, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/getUserInput/{name}/{ticker}/{capital}/{short_window}/{long_window}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)

	public void stockInput(@PathVariable String name, @PathVariable String ticker, @PathVariable double capital,
			@PathVariable int long_window, @PathVariable int short_window) {
		LOG.info("inside the function");
		Stock getStock = stockService.findByTicker(ticker);
		Price currentPrice = priceService.findLatest(getStock, 1).get(0);
		LOG.info("Have got the price");
		int size = (int) (capital / currentPrice.getPrice());
		LOG.info("Starting the strategy");

		int lwindow = (int) (long_window / 10);
		int swindow = (int) (short_window / 10);

		Date date = new Date();
		SimpleStrategy initial_strategy = new SimpleStrategy(-1, name.toLowerCase(), getStock, capital, size, lwindow,
				swindow, date);
		simpleStrategyService.save(initial_strategy);

	}

	@RequestMapping(value = "/stopping/{id}", method = RequestMethod.PUT)
	public void terminate_current_strategy(@PathVariable int id) {
		LOG.info("Terminating all strategies");
		SimpleStrategy strategy = simpleStrategyService.findById(id); 
		Date now = new Date();
		strategy.setStop(now);
		simpleStrategyService.save(strategy);

		

	}

}
