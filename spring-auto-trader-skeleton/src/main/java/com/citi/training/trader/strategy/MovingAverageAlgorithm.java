package com.citi.training.trader.strategy;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.DealsDao;
import com.citi.training.trader.exceptions.EntityNotFoundException;
import com.citi.training.trader.messaging.TradeSender;
import com.citi.training.trader.model.Deals;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.service.DealsService;
import com.citi.training.trader.service.PriceService;
import com.citi.training.trader.service.SimpleStrategyService;
import com.citi.training.trader.service.TradeService;

@Profile("!no-scheduled")
@Component
public class MovingAverageAlgorithm implements StrategyAlgorithm {

	private static final Logger logger = LoggerFactory.getLogger(MovingAverageAlgorithm.class);

	@Autowired
	private TradeSender tradeSender;

	@Autowired
	private PriceService priceService;

	@Autowired
	private SimpleStrategyService strategyService;

	@Autowired
	private TradeService tradeService;

	@Autowired
	private DealsService dealsService;

	@Autowired
	private DealsDao dealsDao;

	Trade trade1 = null;

	@Scheduled(fixedRateString = "${simple.strategy.refresh.rate_ms:5000}")
	public void run() {

		for (SimpleStrategy strategy : strategyService.findAll()) {

			Price currentPrice = priceService.findLatest(strategy.getStock(), 1).get(0);
			double getValueNow = currentPrice.getPrice();
			double ProfitNow = (getValueNow * strategy.getSize()) - strategy.getCapital();
			
			double roiNow = ((getValueNow * strategy.getSize()) - strategy.getCapital())/strategy.getCapital();
			strategy.setROI(roiNow);
			strategy.setProfit(ProfitNow);
			strategyService.save(strategy);

			Trade lastTrade = null;
			try {
				lastTrade = tradeService.findLatestByStrategyId(strategy.getId());
			} catch (EntityNotFoundException ex) {
				logger.debug("No Trades for strategy id: " + strategy.getId());
			}

			if (lastTrade != null) {
				if (lastTrade.getState() == TradeState.WAITING_FOR_REPLY) {
					logger.debug("Waiting for last trade to complete, do nothing");
					continue;
				}


				// account for last trade?
				if (!lastTrade.getAccountedFor() && lastTrade.getState() == TradeState.FILLED) {
					logger.debug("Accounting for Trade: " + lastTrade);

					if (strategy.getWhich_trade() == 0) {
						// insert Trade1-first Trade
						// deals=new Deals(-1, strategy, lastTrade);
						// dealsDao.save(deals);

						trade1 = lastTrade;
						strategy.setWhich_trade(1);
						strategyService.save(strategy);

					}

					else if (strategy.getWhich_trade() == 1) {
						// position is closed-second Trade

						Deals deals = new Deals(-1, strategy, trade1);
						double diff = profitAndLoss(trade1, lastTrade, strategy);
						deals.setTrade2(lastTrade);
						deals.setProfit(diff);
						
						double roi=roi(trade1,lastTrade, strategy);
						deals.setROI(roi);
						
						
						dealsDao.save(deals);
						strategy.setWhich_trade(0);
						strategyService.save(strategy);

					}

					if (strategy.getPrevious() == 1) {
						strategy.takeShortPosition();
						strategy.setClose(0);
						strategyService.save(strategy);
					}

					else if (strategy.getPrevious() == -1) {
						strategy.takeLongPosition();
						strategy.setClose(0);

					}

				}

				logger.info("Saving for account for");
				lastTrade.setAccountedFor(true);
				tradeService.save(lastTrade);

			}

			// TODO: We should be checking for open positions and
			// closing them if necessary
//			if (strategy.getStopped() != null) {
//				continue;
//			}

			logger.debug("Taking strategic action");
			List<Price> long_prices = priceService.findLatest(strategy.getStock(), strategy.getLongWindow());
			List<Price> short_prices = priceService.findLatest(strategy.getStock(), strategy.getShortWindow());
			if (!strategy.hasPosition()) {
				// we have no open position

				// get latest two prices for this stock

				if ((long_prices.size() < strategy.getLongWindow())
						|| ((short_prices.size() < strategy.getShortWindow()))) {
					logger.warn("Unable to execute strategy, not enough price data: " + strategy);
					continue;
				}

				double long_average = long_average(long_prices);
				double short_average = short_average(short_prices);

				if (short_average < long_average) {
					short_less_than_long(strategy);
				} else if (long_average < short_average) {
					long_less_than_short(strategy);
				}
			}

			else if (strategy.hasLongPosition()) {
				long_less_than_short(strategy);
				// closePosition(lastTrade.getPrice() - strategy.getLastTradePrice(), strategy);
				strategy.setClose(1);
				strategyService.save(strategy);

			}

			else if (strategy.hasShortPosition()) {
				short_less_than_long(strategy);
				// closePosition(lastTrade.getPrice() - strategy.getLastTradePrice(), strategy);
				strategy.setClose(1);
				strategyService.save(strategy);

			}

		}
	}

	private void closePosition(double profitLossPerShare, SimpleStrategy strategy) {
		double totalProfitLoss = profitLossPerShare * strategy.getSize();

		logger.debug("Recording profit/loss of: " + totalProfitLoss + " for strategy: " + strategy);
		strategy.addProfitLoss(totalProfitLoss);
		strategy.closePosition();

		if (strategy.getProfit() >= strategy.getExitProfitLoss()) {
			logger.debug("Exit condition reached, exiting strategy");
			strategy.setStopped(new Date());
			strategyService.save(strategy);
		}
	}

	// if short<long
	public void short_less_than_long(SimpleStrategy strategy) {

		boolean flag = true;
		while (flag) {
			List<Price> long_prices = priceService.findLatest(strategy.getStock(), strategy.getLongWindow());
			List<Price> short_prices = priceService.findLatest(strategy.getStock(), strategy.getShortWindow());

			double long_average = long_average(long_prices);
			double short_average = short_average(short_prices);
			logger.info("waiting for long average < short average");

			if (long_average < short_average) {
				flag = false;
			}
		}

		logger.debug("Trading to close position position for strategy: " + strategy);
		makeTrade(strategy, Trade.TradeType.LONG);

		strategy.setPrevious(-1);
		strategyService.save(strategy);
		// stratergy.getPosition-LONG

	}

	// if long<short
	public void long_less_than_short(SimpleStrategy strategy) {

		boolean flag = true;
		while (flag) {

			List<Price> long_prices = priceService.findLatest(strategy.getStock(), strategy.getLongWindow());
			List<Price> short_prices = priceService.findLatest(strategy.getStock(), strategy.getShortWindow());

			double long_average = long_average(long_prices);
			double short_average = short_average(short_prices);

			logger.info("waiting for short average < long_average");
			if (short_average < long_average) {
				flag = false;
			}
		}

		logger.debug("Trading to close positon for strategy: " + strategy);
		makeTrade(strategy, Trade.TradeType.SHORT);

		strategy.setPrevious(1);
		// stragegy.getPosition-SHORT

	}

	// get LongAverage
	public double long_average(List<Price> prices)

	{
		double sum = 0.0;
		for (Price p : prices) {
			sum += p.getPrice();

		}
		double avg = sum / (prices.size());
		return avg;
	}

	// get ShortAverage
	public double short_average(List<Price> prices) {
		double sum = 0.0;
		for (Price p : prices) {
			sum += p.getPrice();

		}
		double avg = sum / (prices.size());
		return avg;
	}

	public double profitAndLoss(Trade trade1, Trade trade2, SimpleStrategy strategy) {

		double diff = (trade2.getPrice() - trade1.getPrice()) * strategy.getSize();
		return diff;
	}
	
	public double roi(Trade trade1, Trade trade2, SimpleStrategy strategy) {
		double diff=profitAndLoss(trade1, trade2, strategy);
		return (diff/trade1.getPrice())*100;
	}

	private double makeTrade(SimpleStrategy strategy, Trade.TradeType tradeType) {
		Price currentPrice = priceService.findLatest(strategy.getStock(), 1).get(0);
		tradeSender.sendTrade(new Trade(currentPrice.getPrice(), strategy.getSize(), tradeType, strategy));
		return currentPrice.getPrice();
	}

}
