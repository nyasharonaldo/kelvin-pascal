package com.citi.training.trader.pricefeed;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class ConygreFeed2 implements PriceFeed {
    
    private static final Logger LOG = LoggerFactory.getLogger(ConygreFeed2.class);

    @Value("${com.citi.training.trader.pricefeed.feed2.url:http://feed.uat.conygre.com}")
    private String priceFeedUrl;

    public List<Price> findLatest(Stock stock, int count) {
        RestTemplate restTemplate = new RestTemplate();

        // Note this is only asking for 1 price per request - inefficient
        String url  = priceFeedUrl + "/API/StockFeed/GetStockPricesForSymbol/" +
                      stock.getTicker() + "?HowManyValues=" + count;

        LOG.info("Requesting price from: [" + url + "]");

        ResponseEntity<String> feedResponse =
                restTemplate.exchange(url,
                                      HttpMethod.GET,
                                      null,
                            new ParameterizedTypeReference<String>() {});

        LOG.info("Received price data: [" + feedResponse.getBody() + "]");


        List<ConygreFeed2Dto> parsedJson = new ArrayList<ConygreFeed2Dto>();
        try {
            parsedJson = new ObjectMapper().readValue(feedResponse.getBody(),
                                                      new TypeReference<List<ConygreFeed2Dto>>() {});
        } catch(Exception ex) {
            LOG.error("Failed to parse price from json: " + feedResponse.getBody());
        }

        List<Price> foundPrices = new ArrayList<Price>();
        for(ConygreFeed2Dto feedRecord: parsedJson) {
            LOG.info("Received price data: " + feedRecord);
            foundPrices.add(feedRecord.toPrice());
        }

        Collections.reverse(foundPrices);
        return foundPrices;
    }
}
