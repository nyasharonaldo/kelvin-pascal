package com.citi.training.trader.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.DealsDao;
import com.citi.training.trader.model.Deals;
import com.citi.training.trader.model.Trade;
import com.google.common.collect.Lists;

@Component
public class DealsService {

	private static final Logger LOG = LoggerFactory.getLogger(StockService.class);
	@Autowired
	private DealsDao dealsDao;

	public List<Deals> findAll() {
		List<Deals> deals = Lists.newArrayList(dealsDao.findAll());
		LOG.info("Found deal: " + deals );
		return deals;
	}
	

	
}
