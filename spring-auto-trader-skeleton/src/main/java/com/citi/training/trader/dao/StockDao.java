package com.citi.training.trader.dao;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.trader.model.Stock;

/**
 * 
 * Stock Dao interface for database operations, that maps Stock Entity to a table
 *
 */
public interface StockDao extends CrudRepository<Stock, Integer> {

    Stock findByTicker(String ticker);

}
