package com.citi.training.trader.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.ColumnDefault;

/**
 * A model object to hold the data for an instance of the SimpleStrategy algorithm.
 *
 * This strategy will trade for maxTrades and then exit.
 * 
 * See {@link com.citi.training.trader.strategy.SimpleStrategyAlgorithm}.
 *
 */
@Entity
public class SimpleStrategy {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    
    private String name;

    @ManyToOne
    private Stock stock;
    
    @ColumnDefault("720")
    private int long_window;
    
    @ColumnDefault("60")
    private int short_window;
    
    private int size;
    
    @Column(nullable=true)
    private double exitProfitLoss;
    
    @ColumnDefault("0")
    private int which_trade; //0-no trade there, 1-trade1 there;

    private Date start;
    
	private Date stop;
    
	@ColumnDefault("0")
    private int currentPosition;
    
    private double capital;
    
    private double lastTradePrice;
    
    @ColumnDefault("0.0")
    private double profit;
    
    @ColumnDefault("0.0")
    private double roi;
    
    private Date stopped; //flag
    
    private int previous;
    
    
    @ColumnDefault("0")
    private int close; //1-if position closed, 0-if false 
    
    public SimpleStrategy() {}

    public SimpleStrategy(int id, String name, Stock stock, double capital, int size, int long_window, int short_window, Date start) {
    	this.id=id;
    	this.name=name;
    	this.stock=stock;
    	this.capital=capital;
    	this.size=size;
    	this.start=start;
    	this.long_window=long_window;
    	this.short_window=short_window;
    }
 
	public SimpleStrategy(int id, Stock stock, double capital, int size,
                          double exitProfitLoss, int currentPosition,
                          double lastTradePrice, double profit, int long_window, int short_window,
                          Date stopped) {
        this.id = id;
        this.stock = stock;
        this.capital=capital;
        this.size=size;
        this.exitProfitLoss = exitProfitLoss;
        this.currentPosition = currentPosition;
        this.lastTradePrice = lastTradePrice;
        this.profit = profit;
        this.long_window=long_window;
        this.short_window=short_window;
        this.stopped = stopped;
    }

    public int getLongWindow()
    {
    	return this.long_window;
    }
    
    public int getShortWindow()
    {
    	return this.short_window;
    }
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Stock getStock() {
        return this.stock;
    }

    public int getSize() {
        return this.size;
    }
    
    public int getPrevious() {
    	return this.previous;
    }
    
    public void setStock(Stock stock) {
        this.stock = stock;
    }


    public void setSize(int size) {
        this.size = size;
    }

    public double getExitProfitLoss() {
        return this.exitProfitLoss;
    }

    public void setExitProfitLoss(double exitProfitLoss) {
        this.exitProfitLoss = exitProfitLoss;
    }

    public int getCurrentPosition() {
        return this.currentPosition;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    public double getLastTradePrice() {
        return this.lastTradePrice;
    }

    public void setLastTradePrice(double lastTradePrice) {
        this.lastTradePrice = lastTradePrice;
    }

    public double getProfit() {
        return this.profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public void setLongWindow(int long_window) {
    	this.long_window=long_window;
    }
    
    public void setShortWindow(int short_window) {
    	this.short_window=short_window;
    }
    
    public Date getStopped() {
        return this.stopped;
    }

    public void setStopped(Date stopped) {
        this.stopped = stopped;
    }


	public void addProfitLoss(double profitLoss) {
        this.profit += profitLoss;
        if(Math.abs(this.profit) >= exitProfitLoss) {
            this.setStopped(new Date());
        }
    }

    public void stop() {
        this.stopped = new Date();
    }

    public boolean hasPosition() {
        return this.currentPosition != 0;
    }

    public boolean hasShortPosition() {
        return this.currentPosition < 0;
    }

    public boolean hasLongPosition() {
        return this.currentPosition > 0;
    }

    public void takeShortPosition() {
        this.currentPosition = -1;
    }

    public void takeLongPosition() {
        this.currentPosition = 1;
    }

    public void closePosition() {
        this.currentPosition = 0;
    }
    
    public void setPrevious(int previous) {
    	this.previous=previous;
    }
    
	public int getClose() {
		return this.close;
	}

	public void setClose(int close) {
		this.close = close;
	}


	public double getCapital() {
		return this.capital;
	}

	public void setCapital(double capital) {
		this.capital = capital;
	}

	public Date getStart() {
		return this.start;
	}

	
	public void setStart(Date start) {
		this.start = start;
	}

	public Date getStop() {
		return this.stop;
	}

	public void setStop(Date stop) {
		this.stop = stop;
	}
	
	
	public double getROI() {
		return this.roi;
	}

	public void setROI(double roi) {
		this.roi=roi;
	}
	
	public void setName(String name){
		this.name=name;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public int getWhich_trade() {
		return which_trade;
	}

	public void setWhich_trade(int which_trade) {
		this.which_trade = which_trade;
	}


	@Override
    public String toString() {
        return "Strategy [id=" + id + ", stock=" + stock +
                ", size=" + size + ", maxTrades=" + exitProfitLoss +
                ", currentPosition=" + currentPosition +
                ", stopped=" + stopped + "]";
    }
}
