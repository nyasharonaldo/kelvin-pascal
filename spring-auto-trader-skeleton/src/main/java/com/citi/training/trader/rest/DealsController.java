package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.Deals;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.service.DealsService;
import com.citi.training.trader.service.StockService;

/**
 * REST Controller for {@link com.citi.training.trader.model.Deals} resource.
 *
 */

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/deals")
public class DealsController {

    private static final Logger LOG =
                    LoggerFactory.getLogger(StockController.class);

    @Autowired
    private DealsService dealsService;

    
    /**
     * This would be used to find all the rows in the deals table
     * @return List This would return a list all the deals object
     */
    @RequestMapping(method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Deals> findAll() {
        LOG.info("findAll()");
        return dealsService.findAll();
    }

}
