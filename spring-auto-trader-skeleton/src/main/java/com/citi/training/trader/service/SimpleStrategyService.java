package com.citi.training.trader.service;

import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.SimpleStrategyDao;
import com.citi.training.trader.exceptions.EntityNotFoundException;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;
import com.google.common.collect.Lists;

@Component
public class SimpleStrategyService {

	  private static final Logger LOG =
              LoggerFactory.getLogger(StockService.class);
	  
    @Autowired
    private SimpleStrategyDao simpleStrategyDao;

    public List<SimpleStrategy> findAll(){
        return Lists.newArrayList(simpleStrategyDao.findAll());
    }

    public SimpleStrategy save(SimpleStrategy strategy) {
        return simpleStrategyDao.save(strategy);
    }

    public SimpleStrategy findLatest() {
        return simpleStrategyDao.findLatest().get(0);
    }
    
    
  public SimpleStrategy findById(int id) {
      return simpleStrategyDao.findById(id).get();
  }
  

}
 
