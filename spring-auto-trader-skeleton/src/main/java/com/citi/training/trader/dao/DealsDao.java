package com.citi.training.trader.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.citi.training.trader.model.Deals;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Trade;

/**
 * 
 * Deals DAO interface for database operations, that maps Deals Entity to a table
 *
 */
public interface DealsDao extends CrudRepository<Deals, Integer> {


}
