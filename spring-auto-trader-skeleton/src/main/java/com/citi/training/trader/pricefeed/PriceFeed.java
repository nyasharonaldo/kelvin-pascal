package com.citi.training.trader.pricefeed;

import java.util.List;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

public interface PriceFeed {

    List<Price> findLatest(Stock stock, int count);
}
