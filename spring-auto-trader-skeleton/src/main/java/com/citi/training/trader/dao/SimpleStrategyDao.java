package com.citi.training.trader.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;

/**
 * 
 * SimpleStrategy interface for database operations, that maps SimpleStrategy Entity to a table
 *
 */
public interface SimpleStrategyDao extends CrudRepository<SimpleStrategy, Integer> {
	
	String query="SELECT * FROM simple_strategy ORDER by id DESC LIMIT 1;";
	@Query(value = query, nativeQuery = true)
	List <SimpleStrategy> findLatest();

}
