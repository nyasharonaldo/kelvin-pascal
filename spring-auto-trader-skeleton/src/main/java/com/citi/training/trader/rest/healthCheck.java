package com.citi.training.trader.rest;
import javax.annotation.Priority;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Controller for {@link com.citi.training.trader.model.SimpleStrategy}
 * resource.
 *
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/status")
public class healthCheck {
	
	
	@RequestMapping(method = RequestMethod.GET)
	public String healthCheck() {
		return "Status OK";
	}

}
