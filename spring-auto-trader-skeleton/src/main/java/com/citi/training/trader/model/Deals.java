package com.citi.training.trader.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.ColumnDefault;

/**
 * 
 * Maps the variables to a Deals table for storing a completed deal
 *
 */
@Entity
public class Deals {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	SimpleStrategy stratergy;

	@OneToOne
	@ColumnDefault("null")
	Trade trade1;

	@OneToOne
	@ColumnDefault("null")
	Trade trade2;

	double profit;
	
	double roi;

	 public Deals() {}
	 
	 
	public Deals(int id, SimpleStrategy stratergy, Trade trade1) {
		// open position
		this.id = id;
		this.stratergy=stratergy;
		this.trade1 = trade1;
	}

	public Deals(int id, Trade trade2, double profit) {
		// closePosition
		this.id = id;
		this.trade2 = trade2;
		this.profit = profit;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public SimpleStrategy getStratergy() {
		return stratergy;
	}

	public void setStratergy(SimpleStrategy stratergy) {
		this.stratergy = stratergy;
	}

	public Trade getTrade1() {
		return trade1;
	}

	public void setTrade1(Trade trade1) {
		this.trade1 = trade1;
	}

	public Trade getTrade2() {
		return trade2;
	}

	public void setTrade2(Trade trade2) {
		this.trade2 = trade2;
	}

	public double getProfit() {
		return profit;
	}

	public void setProfit(double profit) {
		this.profit = profit;
	}
	
	public double getROI() {
		return roi;
	}
	public void setROI(double roi) {
		this.roi=roi;
	}
	
	

}
