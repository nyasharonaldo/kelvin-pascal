package com.citi.training.trader.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trader.model.Deals;

/**
 * Integration test for Stock REST Interface.
 *
 * Makes HTTP requests to {@link com.citi.training.stockr.rest.TradeController}.
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"h2", "no-scheduled"})
public class DealsControllerIntegrationTests {

	private static final Logger logger = LoggerFactory.getLogger(TradeControllerIntegrationTests.class);

	@Autowired
	private TestRestTemplate restTemplate;

	private static final String tradeBasePath = "/api/v1/deals";

	@Test
	public void findAll_returnsList() {

		ResponseEntity<List<Deals>> findAllResponse = restTemplate.exchange(tradeBasePath, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Deals>>() {
				});

		assertEquals(HttpStatus.OK, findAllResponse.getStatusCode());

	}
}
