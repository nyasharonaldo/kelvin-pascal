package com.citi.training.trader.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.ColumnDefault;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

public class SimpleStrategyTests {
    private int id=1;
    private String name="ma";
    
    private Stock stock=new Stock(1, "ibm");
    private int long_window=720;
    private int short_window=60;
    private int size=1000;
    
    private double exitProfitLoss=0;
    private int which_trade=0; //0-no trade there, 1-trade1 there;
    private Date start=new Date();
	private Date stop=new Date();
    
    private int currentPosition=0;
    
    private double capital=0;
    
    private double lastTradePrice=0;
    
    private double profit=0.0;
    
    private double roi=0.0;
    
    private Date stopped=null; //flag
    
    private int previous=0;;
    
    private int close=0; //1-if position closed, 0-if false

  
    @Test
    
    public void test_fullConstructor() {
    	SimpleStrategy strategy=new SimpleStrategy(id, stock, capital,size,
        exitProfitLoss,currentPosition,
        lastTradePrice, profit, long_window, short_window,
        stopped);
    	
    	assertEquals("Strategy Id should equal value given in constructor",
                id, strategy.getId());
    	
    	assertEquals("Strategy Stock should equal value given in constructor",
                stock, strategy.getStock());
    	
    	assertEquals("Strategy captial should equal value given in constructor",
                capital, strategy.getCapital(),0.001);
    	
    	assertEquals("Strategy size should equal value given in constructor",
                size, strategy.getSize());
    	
    	assertEquals("Strategy exitProfitLoss should equal value given in constructor",
                exitProfitLoss, strategy.getExitProfitLoss(),0.001);
    	
    	assertEquals("Strategy current Position should equal value given in constructor",
                currentPosition, strategy.getCurrentPosition());
    	
    	assertEquals("Strategy lastTradePrice should equal value given in constructor",
                lastTradePrice, strategy.getLastTradePrice(),0.001);
    	
    	assertEquals("Strategy profit should equal value given in constructor",
                profit, strategy.getProfit(),0.001);
    	
    	assertEquals("Strategy Long Window should equal value given in constructor",
                long_window, strategy.getLongWindow());
    	
    	assertEquals("Strategy Short Window should equal value given in constructor",
                short_window, strategy.getShortWindow());
    	
    	assertEquals("Strategy current Position should equal value given in constructor",
                stopped, strategy.getStopped());
    	
    	
    }
    
    public void testConstuctor() {
    	SimpleStrategy strategy=new SimpleStrategy(id, name, stock, capital, size, long_window, short_window, start);
    	
    	assertEquals("Strategy id equal value given in constructor",
                id, strategy.getId());
    	
    	assertEquals("Strategy stock should equal value given in constructor",
                stock, strategy.getStock());
    	assertEquals("Strategy capital should equal value given in constructor",
                capital, strategy.getCapital(),0.001);
    	assertEquals("Strategy Long Window should equal value given in constructor",
                long_window, strategy.getLongWindow());
    	assertEquals("Strategy Short Window should equal value given in constructor",
                short_window, strategy.getShortWindow());
    	assertEquals("Strategy Start should equal value given in constructor",
                start, strategy.getStart());
    	
    	}
    }
    
