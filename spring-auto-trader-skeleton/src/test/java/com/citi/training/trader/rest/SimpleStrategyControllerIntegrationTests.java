package com.citi.training.trader.rest;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trader.model.Deals;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"h2", "no-scheduled"})
public class SimpleStrategyControllerIntegrationTests {

	private static final Logger logger = LoggerFactory.getLogger(TradeControllerIntegrationTests.class);

	@Autowired
	private TestRestTemplate restTemplate;

	private static final String strategyBasePath = "/api/v1/strategies";

	@Test
	public void testStrategyController() {

		ResponseEntity<List<SimpleStrategy>> findAllResponse = restTemplate.exchange(strategyBasePath, HttpMethod.GET,
				null, new ParameterizedTypeReference<List<SimpleStrategy>>() {
				});

		assertEquals(HttpStatus.OK, findAllResponse.getStatusCode()); // testing for showing all the values;

		// testing to see if create is working properly
		
		int id=1000;
		String name="ma";
		Stock stock=null;
		double capital=2000.0;
		int size=20;
		int long_window=200;
		int short_window=20;
		Date date=null;
		
		SimpleStrategy strategy = new SimpleStrategy(id, name, stock, capital, size, long_window, short_window, date);
		ResponseEntity<SimpleStrategy> createTradeResponse = restTemplate.postForEntity(strategyBasePath, strategy,
				SimpleStrategy.class);

		
		logger.debug("Create SimpleStrategy: " + createTradeResponse.getBody());

		assertEquals(HttpStatus.CREATED, createTradeResponse.getStatusCode());
		
		assertEquals("checking strategy name",
                name, createTradeResponse.getBody().getName());
		
		assertEquals("checking stock objecg",
                stock, createTradeResponse.getBody().getStock());
		
		assertEquals("checking strategy capital",
                capital, createTradeResponse.getBody().getCapital(),0.001);
		
		assertEquals("checking strategy size",
                size, createTradeResponse.getBody().getSize());
		
		assertEquals("checking strategy long window",
                long_window, createTradeResponse.getBody().getLongWindow());
		
		assertEquals("checking short_window",
                short_window, createTradeResponse.getBody().getShortWindow());
		
		//get by id
		
        ResponseEntity<SimpleStrategy> createdResponse =
                restTemplate.postForEntity(strategyBasePath,
                                           strategy, SimpleStrategy.class);

        assertEquals(HttpStatus.CREATED, createdResponse.getStatusCode());

        SimpleStrategy foundStock = restTemplate.getForObject(
                                strategyBasePath + "/" + createdResponse.getBody().getId(),
                                SimpleStrategy.class);

        assertEquals(createdResponse.getBody().getId(), foundStock.getId());
        
     
        String path=strategyBasePath+"/getUserInput/ma/ibm/200.0/20/200";
        ResponseEntity<SimpleStrategy> getUserResponse = restTemplate.exchange(
        		path,
        		HttpMethod.POST, null,
        		new ParameterizedTypeReference<SimpleStrategy>() {});
        logger.info("Checking for user Response: " + getUserResponse.getBody());
        
        
        //find latest by value
        
        String latestPath=strategyBasePath+"/findLatest";
        ResponseEntity<SimpleStrategy> getLatestResponse = restTemplate.exchange(
        		latestPath,
        		HttpMethod.POST, null,
        		new ParameterizedTypeReference<SimpleStrategy>() {});
        logger.info("Checking for user Response: " + getLatestResponse.getBody());
        
        assertEquals(HttpStatus.OK, getLatestResponse.getStatusCode());
        
        //check if stop is working
        
        String stopPath=strategyBasePath+"/stopping/1";
        ResponseEntity<SimpleStrategy> getStopResponse = restTemplate.exchange(
        		stopPath,
        		HttpMethod.PUT, null,
        		new ParameterizedTypeReference<SimpleStrategy>() {});
        logger.info("Checking for when stratergy is stopped by id: " + getStopResponse.getBody());
        
        assertEquals(HttpStatus.OK, getStopResponse.getStatusCode());
        
        
	}

	}

