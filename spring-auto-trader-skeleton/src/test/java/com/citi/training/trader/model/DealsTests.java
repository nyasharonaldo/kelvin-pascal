package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;

import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.ColumnDefault;
import org.junit.Test;

public class DealsTests {
	
	private int id=1;

	private SimpleStrategy strategy=null;

	private Trade trade1=null;

	private Trade trade2=null;

	private double profit=0.0;
	
	private double roi=0.0;
	
	@Test
	
	public void checkFirstConstructor() {
		
		Deals deal=new Deals(id, strategy, trade1);
		assertEquals("Create id should match the constructor",
                id, deal.getId());
		
		assertEquals("Create stratergy should match the constructor",
                strategy, deal.getStratergy());
		
		assertEquals("Create trade1 should match the constructor",
		
                strategy, deal.getTrade1());	
	
	}
	
	public void checkSecondConstructor() {
		
		Deals deal=new Deals(id, trade2, profit);
		assertEquals("Create id should match the constructor",
                id, deal.getId());
		
		assertEquals("Create stratergy should match the constructor",
                trade2, deal.getTrade2());
		
		assertEquals("Create trade1 should match the constructor",
                profit, deal.getProfit(),0.001);	
	
	}
	
	


}
