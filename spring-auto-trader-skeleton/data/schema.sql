-- MySQL dump 10.13  Distrib 5.7.14, for Win64 (x86_64)
--
-- Host: localhost    Database: traderdb
-- ------------------------------------------------------
-- Server version	5.7.14-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `traderdb`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `traderdb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `traderdb`;

--
-- Table structure for table `deals`
--

DROP TABLE IF EXISTS `deals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profit` double NOT NULL,
  `roi` double NOT NULL,
  `stratergy_id` int(11) DEFAULT NULL,
  `trade1_id` int(11) DEFAULT NULL,
  `trade2_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4iibfufnorsx1axg052hwn2f9` (`stratergy_id`),
  KEY `FK9qmbku3il8eaen8prob9r2jnp` (`trade1_id`),
  KEY `FK5kbntpfbac7vfhtu1lecv43t2` (`trade2_id`),
  CONSTRAINT `FK4iibfufnorsx1axg052hwn2f9` FOREIGN KEY (`stratergy_id`) REFERENCES `simple_strategy` (`id`),
  CONSTRAINT `FK5kbntpfbac7vfhtu1lecv43t2` FOREIGN KEY (`trade2_id`) REFERENCES `trade` (`id`),
  CONSTRAINT `FK9qmbku3il8eaen8prob9r2jnp` FOREIGN KEY (`trade1_id`) REFERENCES `trade` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deals`
--

LOCK TABLES `deals` WRITE;
/*!40000 ALTER TABLE `deals` DISABLE KEYS */;
/*!40000 ALTER TABLE `deals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `simple_strategy`
--

DROP TABLE IF EXISTS `simple_strategy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `simple_strategy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `capital` double NOT NULL,
  `close` int(11) NOT NULL DEFAULT '0',
  `current_position` int(11) NOT NULL DEFAULT '0',
  `exit_profit_loss` double DEFAULT NULL,
  `last_trade_price` double NOT NULL,
  `long_window` int(11) NOT NULL DEFAULT '720',
  `name` varchar(255) DEFAULT NULL,
  `previous` int(11) NOT NULL,
  `profit` double NOT NULL DEFAULT '0',
  `roi` double NOT NULL DEFAULT '0',
  `short_window` int(11) NOT NULL DEFAULT '60',
  `size` int(11) NOT NULL,
  `start` datetime DEFAULT NULL,
  `stop` datetime DEFAULT NULL,
  `stopped` datetime DEFAULT NULL,
  `which_trade` int(11) NOT NULL DEFAULT '0',
  `stock_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKqxxthvr4vkuw8nr5aeguyen38` (`stock_id`),
  CONSTRAINT `FKqxxthvr4vkuw8nr5aeguyen38` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `simple_strategy`
--

LOCK TABLES `simple_strategy` WRITE;
/*!40000 ALTER TABLE `simple_strategy` DISABLE KEYS */;
/*!40000 ALTER TABLE `simple_strategy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stock`
--

DROP TABLE IF EXISTS `stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticker` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stock`
--

LOCK TABLES `stock` WRITE;
/*!40000 ALTER TABLE `stock` DISABLE KEYS */;
INSERT INTO `stock` VALUES (1,'ibm');
INSERT INTO `stock` VALUES (2,'aapl');
INSERT INTO `stock` VALUES (3,'msft');
INSERT INTO `stock` VALUES (4,'nflx');
INSERT INTO `stock` VALUES (5,'c');
INSERT INTO `stock` VALUES (6,'goog');
INSERT INTO `stock` VALUES (7,'amzn');
INSERT INTO `stock` VALUES (8,'fb');
INSERT INTO `stock` VALUES (9,'hpq');
INSERT INTO `stock` VALUES (10,'intc');
/*!40000 ALTER TABLE `stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trade`
--

DROP TABLE IF EXISTS `trade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accounted_for` bit(1) NOT NULL,
  `last_state_change` datetime DEFAULT NULL,
  `price` double NOT NULL,
  `size` int(11) NOT NULL,
  `state` int(11) DEFAULT NULL,
  `trade_type` int(11) DEFAULT NULL,
  `strategy_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8pmjp2f325a27rgn5rrw7ir0g` (`strategy_id`),
  CONSTRAINT `FK8pmjp2f325a27rgn5rrw7ir0g` FOREIGN KEY (`strategy_id`) REFERENCES `simple_strategy` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trade`
--

LOCK TABLES `trade` WRITE;
/*!40000 ALTER TABLE `trade` DISABLE KEYS */;
/*!40000 ALTER TABLE `trade` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-02 19:45:21
